import glob
import subprocess
from os.path import expanduser, isdir, exists, join
from os import sep
from os import walk
import argparse
from sys import exit

parser = argparse.ArgumentParser()
parser.add_argument("src", help="source folder")
parser.add_argument("dst", help="destination folder")

args = parser.parse_args()

from_path = args.src
path = args.dst

#from_path+='*'

if path[0] == '~':
    path = expanduser("~") + path[:1]
if from_path[0] == '~':
    from_path = expanduser("~") + from_path[:1]

if not isdir(path):

    proc = subprocess.Popen('mkdir '+path , shell=True)

directories = []
filenames = []
#
for root, subfolders, files in  walk(from_path):
    for f in files:
        filenames.append(join(root, f))

#

#for filename in glob.glob(from_path):
#
for filename in filenames:
#
    if filename.rfind('.'):
        #extension
        ext = filename[filename.rfind('.')+1:]



        if ext not in directories:
            directories.append(ext)
            p = path+sep+ext
            print p
            if isdir(p):
                directories.append(ext)
            else:
                proc = subprocess.Popen('mkdir '+p, shell=True)


        short_name = filename[filename.rfind(sep):]

        i = 0
        while exists(path + sep +ext+sep+short_name):
            i+=1

            short_name = short_name[:short_name.rfind('.')]+'_'+str(i)+short_name[short_name.rfind('.'):]





        proc = subprocess.Popen('mv '+filename+' '+path + sep +ext+sep+short_name, shell=True)


    else:
        if isdir(filename):
            continue
        #if not "no_extension" in directories:
        if not isdir(path + sep +'no_extensions'):

            proc = subprocess.Popen('mkdir '+path + sep +'no_extensions', shell=True)

        short_name = filename[filename.rfind(sep):]
        i = 0

        while exists(path + sep +"no_extensions"+sep+short_name):
            i+=1

            short_name = short_name+'_'+str(i)

        proc = subprocess.Popen('mv '+filename+' '+path + sep +"no_extensions"+sep+short_name, shell=True)


